class CompMovimientoDm extends Polymer.Element {

  static get is() {
    return 'comp-movimiento-dm';
  }

  static get properties() {
    var cuenta= JSON.parse(sessionStorage.getItem("cuenta"));
    return {
      _host: {
        type: String,
        value: 'http://clienteapi.us-west-1.elasticbeanstalk.com'
      },
      _path: {
        type: String,
        value: 'movimientos/'+cuenta.id
      },

      _body: {
        type: Object,
        value: {}
      }
    };
  }
  getAllMovimientos() {
    console.info("cells-crud-dm::getAllMovimientos......")
    this.$.cellsCrudGetDP.generateRequest();
  }
  _onGetResponse(evt) {
    console.info("cells-crud-dm::_onGetResponse... begin");
    let detail = evt.detail || null;
    if (detail !== null) {
      console.info('cells-crud-dm::_onGetResponse::evt.detail: ', detail);
     /*
      var array1 = detail;
      var saldo=0;      
      array1.forEach(function(element) {        
        saldo=saldo+element.valor;
      });
      sessionStorage.setItem("saldo",saldo);
    */
      this.dispatchEvent(new CustomEvent('show-movimientos', {composed: true, bubbles: true, detail: detail}));
  
      this.dispatchEvent(new CustomEvent('open-spinner', {composed: true, bubbles: true, detail: false}));
    } else {
      console.info('cells-crud-dm::_onGetResponse::evt.detail: null');
    }
    console.info("cells-crud-dm::_onGetResponse... end");
  }
  _onGetError(err) {
    let detail = err.detail || null;

    if (detail !== null) {
      console.error('cells-crud-dm::_onGetError::err.detail: ', detail);
    }

    this.dispatchEvent(new CustomEvent('open-spinner', {composed: true, bubbles: true, detail: false}));
  }

  addMovimiento(movimiento) {
    console.info('cells-crud-dm::addMovimiento.... begin');
    this.set('_path', 'movimiento');
    this.set('_body',movimiento);
    this.$.cellsCrudPostDP.generateRequest();
    console.info('cells-crud-dm::addMovimiento.... end');
  }
  _onPostResponse(evt) {
    console.info('cells-crud-dm::_onPostResponse.... begin');
    let detail = evt.detail || null;
    if (detail !== null) {
      var cuenta= JSON.parse(sessionStorage.getItem("cuenta"));     
      this.set('_path', 'cuenta/cliente/'+cuenta.idCliente);
      this.$._body = {};
      this.$.cellsGetCuentaDP.generateRequest();
    }
    console.info('cells-crud-dm::_onPostResponse.... end');
  }
  _onPostError(err) {
    let detail = err.detail || null;

    if (detail !== null) {
      console.error('cells-crud-dm::_onPostError::err.detail: ', detail);
    }
  }

  _onGetCuentaResponse(evt) {
    console.info("cells-crud-dm::_onGetCuentaResponse... begin");
    let detail = evt.detail || null;
    if (detail !== null) {
      console.info('cells-crud-dm::_onGetCuentaResponse::evt.detail: ', detail);
  
      this.dispatchEvent(new CustomEvent('show-cuentas', {composed: true, bubbles: true, detail: detail}));
    } else {
      console.info('cells-crud-dm::_onGetCuentaResponse::evt.detail: null');
    }
    console.info("cells-crud-dm::_onGetCuentaResponse... end");
  }
}

customElements.define(CompMovimientoDm.is, CompMovimientoDm);
